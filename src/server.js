const express = require('express')
const mongoose = require('mongoose')
const bodyParse = require('body-parser')
const app = express()

mongoose.connect('mongodb://alexgcunha:alexcunha@localhost:27017/bitx', {useNewUrlParser: true})

app.use(bodyParse.json());
app.use(express.json())
app.use(require('./routes'))

app.listen(3000)