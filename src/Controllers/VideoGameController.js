const VideoGame = require('../Model/video-game');

class VideoGames {
  async  cadastrar(requeste, response){
        const {name, company} = requeste.body

        const data = await VideoGame.create({
            name,
            company

        })
        
        response.json({
            sucess:true,
            data
        })
    }
    async listar(requeste, response){
        const data = await VideoGame.find()
        response.json({
            sucess: true,
            data
        })
    }

}

module.exports = new VideoGames();