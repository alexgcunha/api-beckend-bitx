const Jogo = require('../Model/jogo')
const VideoGame = require('../Model/video-game')

class JogoController{
    async cadastrar(requeste, response){
        try{
            var {name, _id} = await VideoGame.findById(requeste.params.id)
            var console_name = name
            var { name } = requeste.body
            
    
            const data = await Jogo.create({
                name,
                console_name,
                console_id: _id
            })
            
    
            response.status(201).json({
                sucess: true,
                data
            })

        }catch(error){
            
            response.status(202).json({message:'Error no Create - Post Jogo'})
        }
    }

    async listar(requeste, response){
        try{

            const data = await Jogo.find()
            response.status(200).json({
                sucess: true,
                data
            })

        }catch(error){
            response.status(404).json({message:'Error  List '})
        }
    }
    async listarGameConsole(requeste, response){
        try {

            const video = requeste.params.video
            const jogo = await Jogo.find({console_name: video})
            
            const data = jogo.map(jogos => {
                return jogos.name
            })
            
            response.json({
                sucess: true,
                data
            })

        
        }catch(error){
            response.status(404).json({message:'Error List Jogo per Console'})
        }




    }
}


module.exports = new JogoController()