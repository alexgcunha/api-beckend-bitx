const express = require('express')
const routes = express.Router();

const JogoController = require('./Controllers/JogoController')
const VideoGames = require('./Controllers/VideoGameController')



routes.post('/Video-game', VideoGames.cadastrar)
routes.get('/Video-game', VideoGames.listar)
routes.post('/Jogos/:id', JogoController.cadastrar)
routes.get('/Jogos', JogoController.listar)
routes.get('/Jogos/:video', JogoController.listarGameConsole)



module.exports = routes;