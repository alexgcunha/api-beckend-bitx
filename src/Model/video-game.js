const mongoose = require('mongoose');

const VideoGame = new mongoose.Schema({
    name: String,
    company: String
});

module.exports = mongoose.model('VideoGame', VideoGame);