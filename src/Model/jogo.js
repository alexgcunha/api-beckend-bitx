const mongoose = require('mongoose');

const Jogo = new mongoose.Schema({
    name: String,
    console_name: String,
    console_id: String
});

module.exports = mongoose.model('Jogo', Jogo);