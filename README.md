# API RESTFULL 
# Test da BitX

- Deselvolver uma API para cadastro e organização de uma coleção de jogos de Vídeo Game,
seguinte especificações:

- Uma rota para cadastro de vídeo-game
- Uma rota para cadastro de jogos
- Uma rota para listar os jogos cadastrados 
- Uma rota para listar os vídeo-games cadastrados
- Uma rota para visualizar jogos cadastrados por vídeo-games


# Pré-requisitos 

- Desenvolver a aplicação em Nodejs utilizando o framework Express
- Utilizar o banco de dados MongoDB
- Versionar o processo de desenvolvimento utilizando a ferramenta GIT

# Utilizar boas práticas de API REST

- Utilizar os status corretos 
- Não colocar barrar no final de um endpoint

# Ferramentas

- Express
- Mongoose
- nodemon

# Tarefas a seguir 

- Iniciar o servidor - ok
- Conectar no Banco de Dados mongodb - ok
- Separar o projeto em Model e Controllers - ok
- Criar as SCHEMA no Model - Jogo - Games - ok
- Criar os controllers GET - POST - ok
- 

# Resultado

<img src="https://gitlab.com/alexgcunha/api-beckend-bitx/raw/master/readme/Captura%20de%20tela%20de%202019-08-09%2022-37-45.png" width="600" height="500" />
<img src="https://gitlab.com/alexgcunha/api-beckend-bitx/raw/master/readme/Captura%20de%20tela%20de%202019-08-09%2022-38-16.png" width="600" height="500" />